﻿using Microsoft.AspNetCore.Mvc;
using System.Security.Cryptography;
using System.Text;

[ApiController]
[Route("[controller]/[action]")]
public class UsersController : ControllerBase
{
    static List<User> users = new List<User>();

    [HttpGet]
    public List<User> Users() => users;

    [HttpPost]
    public void Register(Register login)
    {
        var newUser = new User
        {
            UserName = login.UserName,
            PasswordHash = HashWithSHA256(login.Password),
        };

        users.Add(newUser);
    }

    [HttpPost]
    public string Login(Login login)
    {
        var user = users.FirstOrDefault(x => x.UserName == login.UserName);

        if(user is null)
        {
            return "tài khoản không tồn tại";
        }

        if(user.PasswordHash == HashWithSHA256(login.Password))
        {
            return "ngon lành";
        }

        return "pass word ngu người";
    }

    string HashWithSHA256(string input)
    {
        using SHA256 sha256Hash = SHA256.Create();

        var inputBytes = Encoding.UTF8.GetBytes(input);
        byte[] bytes = sha256Hash.ComputeHash(inputBytes);

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < bytes.Length; i++)
        {
            builder.Append(bytes[i].ToString("x2"));
        }
        return builder.ToString();
    }

}

public class User
{
    public string UserName { get; set; }
    public string PasswordHash { get; set; }
}

public class Login
{
    public string UserName { get; set; }
    public string Password { get; set; }
}

public class Register
{
    public string UserName { get; set; }
    public string Password { get; set; }
}